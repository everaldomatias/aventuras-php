<?php session_start(); ?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title>Aventuras - Criar Novo Usuario</title>
	</head>
	<body>
		<h2>Criar Novo Usuário</h2>
		<?php
		//echo password_hash( '826414', PASSWORD_DEFAULT );
		  	if ( isset( $_SESSION['msg'] ) ) {  
		  		echo $_SESSION['msg'];
		  		unset( $_SESSION['msg'] );
		  	}
		?>
		<form action="create-new-user.php" method="POST">
			<label for="">E-mail</label>
			<input type="text" name="new_email" placeholder="Digite seu e-mail (ele será usado para validar seu cadastro)"><br><br>
			<label for="">Usuário</label>
			<input type="text" name="new_user" placeholder="Digite o seu usuário"><br><br>
			<label for="">Senha</label>
			<input type="password" name="new_pass" placeholder="Digite a sua senha"><br><br>
			<input type="submit" name="btnCreateUser" value="Criar">
			<input type="hidden" name="createUser" value="create">
		</form>
	</body>
</html>