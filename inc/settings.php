<?php

// Load connection to database
include_once( "inc/config.php" );

// Load creator tables
include_once( "inc/create-db.php" );

// Load functions file
include_once( "functions.php" );