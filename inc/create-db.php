<?php

include_once( "config.php" );

/**
 * Create table "users" if not exist
 */

if ( ! $conn->query ( "DESCRIBE users" ) ) {
	// SQL to create table "users"
	$sql = "CREATE TABLE users (
		id INT(6) AUTO_INCREMENT PRIMARY KEY, 
		username VARCHAR(50) NOT NULL,
		email VARCHAR(50) NOT NULL,
		pass VARCHAR(30) NOT NULL,
		complete_name VARCHAR(50) NOT NULL,
		short_name VARCHAR(50) NOT NULL,
		last_login TIMESTAMP,
		user_since TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	)";

	if ( ! $conn->query( $sql ) === TRUE ) {
	    echo "Error creating table: " . $conn->error;
	}

}

/**
 * Create table "users_validation" if not exist
 */

if ( ! $conn->query ( "DESCRIBE users_validation" ) ) {
	// SQL to create table "users_validation"
	$sql = "CREATE TABLE users_validation (
		id INT(6) AUTO_INCREMENT PRIMARY KEY, 
		username VARCHAR(50) NOT NULL,
		email VARCHAR(50) NOT NULL,
		pass VARCHAR(128) NOT NULL,
		lnkvldtn VARCHAR(128) NOT NULL,
		creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	)";

	if ( ! $conn->query( $sql ) === TRUE ) {
	    echo "Error creating table: " . $conn->error;
	}

}

/**
 * Create table "records" if not exist
 */

 if ( ! $conn->query ( "DESCRIBE records" ) ) {
	// SQL to create table "records"
	$sql = "CREATE TABLE records (
		id INT(6) AUTO_INCREMENT PRIMARY KEY, 
		date_action TIMESTAMP,
		description VARCHAR(50) NOT NULL,
		value VARCHAR(30) NOT NULL,
		km VARCHAR(30) NOT NULL,
		category VARCHAR(50) NOT NULL,
		image VARCHAR(100) NOT NULL,
		date_record TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	)";

	if ( ! $conn->query( $sql ) === TRUE ) {
	    echo "Error creating table: " . $conn->error;
	}

}

/**
 * Create table "settings" if not exist
 */

if ( ! $conn->query ( "DESCRIBE settings" ) ) {
	// SQL to create table "settings"
	$sql = "CREATE TABLE settings (
		id INT(6) AUTO_INCREMENT PRIMARY KEY, 
		initial_km VARCHAR(50) NOT NULL,
		current_km VARCHAR(30) NOT NULL,
		shop_date VARCHAR(30) NOT NULL,
		shop_value VARCHAR(30) NOT NULL,
		installed BIT,
		date_setting TIMESTAMP DEFAULT CURRENT_TIMESTAMP
	)";

	if ( ! $conn->query( $sql ) === TRUE ) {
	    echo "Error creating table: " . $conn->error;
	}

}

/**
 * Create table "lost_password" if not exist
 */

 if ( ! $conn->query ( "DESCRIBE lost_password" ) ) {
	// SQL to create table "lost_password"
	$sql = "CREATE TABLE lost_password (
		user VARCHAR(255) NOT NULL,
		confirmation VARCHAR(40) NOT NULL,
		KEY(user, confirmation)
	)";

	if ( ! $conn->query( $sql ) === TRUE ) {
	    echo "Error creating table: " . $conn->error;
	}

}

//$conn->close();