<?php
	session_start();
	
	// Required Settings
    include_once( "inc/settings.php" );
    
  	$btnCreateUser = filter_input( INPUT_POST, 'btnCreateUser', FILTER_SANITIZE_STRING );
  	
  	if ( $btnCreateUser ) {

  		$new_email  = filter_input(     INPUT_POST, 'new_email',    FILTER_SANITIZE_EMAIL );
  		$new_user   = filter_input(     INPUT_POST, 'new_user',     FILTER_SANITIZE_STRING );
  		$new_pass   = filter_input(     INPUT_POST, 'new_pass',     FILTER_SANITIZE_STRING );

  		if ( ! empty( $new_user ) && ! empty( $new_email ) )  {
  		
  		    if ( empty( $new_pass ) ) {
  		        $new_pass = password_hash( random_string(8), PASSWORD_DEFAULT );
  		    } else {
  		        $new_pass = password_hash( $new_pass, PASSWORD_DEFAULT );
  		    }
  			
  			$search_user = "SELECT id, username, email, pass FROM users_validation WHERE username='$new_user' LIMIT 1";
  			$result_search_user = mysqli_query( $conn, $search_user );
  			
  			$search_user_on_validation = "SELECT id, username, email, pass FROM users_validation WHERE username='$new_user' LIMIT 1";
  			$result_search_user_on_validation = mysqli_query( $conn, $search_user_on_validation );
 			
  			if ( mysqli_num_rows( $result_search_user ) == 0 && mysqli_num_rows( $result_search_user_on_validation ) == 0 ) {

            	if ( isset( $_POST['createUser'] ) && $_POST['createUser'] == "create" ) {
            	
                    $lnkvldtn = random_string( 128 );

        			$createUser = "INSERT INTO users_validation (`username`, `email`, `pass`, `lnkvldtn`) VALUES ('$new_user', '$new_email', '$new_pass', '$lnkvldtn')";
        			if ( mysqli_query( $conn, $createUser ) ) {
        			
                        $subject_preferences = array(
                            "line-length" => 76,
                            "line-break-chars" => "\r\n"
                        );
                        
                        $header = "Content-type: text/html; charset=utf-8 \r\n";
                        $header .= "From: Oscar <oscar@eve14.com.br> \r\n";
                        $header .= "MIME-Version: 1.0 \r\n";
                        $header .= "Content-Transfer-Encoding: 8bit \r\n";
                        $header .= "Date: " . date("r (T)") . " \r\n";
                        $mail_subject = "(( Oscar )) - Link de ativação da sua conta";

                        $message = 'Sua conta está quase pronta, <a href=http://oscar.eve14.com.br/lnkvldtn.php?lnkvldtn=' . $lnkvldtn . '&u=' . $new_user . '&e=' . $new_email . '>clique aqui</a> para ativá-la e então começar a registrar tudo sobre o seu veículo.';
                  
                        $sendMail = mail( 'everaldo@agpagencia.com.br', $mail_subject, $message, $header );

						if ( ! $sendMail ) {
							/**
							 * Todo: Enviar a mensagem de erro para a session
							 */
							$errorMessage = error_get_last()['message'];
						}

        				$_SESSION['msgCreateUser'] = '<div class="col-lg-12 alert alert-success"><strong>Pronto.</strong> Usuário cadastrado com sucesso. Acesse seu e-mail e ative sua conta!</div>';
						header("Location: new-user.php");
					} else {
						echo "Error: " . $createUser . "<br>" . mysqli_error( $conn );
        				$_SESSION['msgCreateUser'] = '<div class="col-lg-12 alert alert-danger"><strong>Erro ao cadastrar usuário.</strong> Tente novamente.</div>';
        			}
            
            	} else {
            		$_SESSION['msg_cadastro'] = '?';
            	}
  				
  			} elseif( $result_search_user_on_validation ) {
                
                $_SESSION['msg'] = "O usuário foi criado e está em nossa fila de validação. Por favor verifique seu e-mail e clique no link para ativação da sua conta.";
                header("Location: new-user.php");
  			   
  			} else {
  			
                $_SESSION['msg'] = "O usuário e/ou e-mail já estão registrados em nosso banco de dados. Por favor, tente recuperar sua senha ou crie um novo usuário.";
                header("Location: new-user.php");
                
            }

  		} else {
  		
  			$_SESSION['msg'] = "Dados incorretos, por favor preencha todos os campos e tente novamente.";
  			header("Location: new-user.php");
  			
  		}
  	} else {
  	
  		$_SESSION['msg'] = "Página não encontrada";
  		header("Location: login.php");
  		
  	}
  	
  	$conn->close();