# Aventuras PHP

Projeto de estudo e desenvolvimento de um sistema para registro de atividades de um veículo.

## Modelo do arquivo inc/config.php

```
<?php
$servername = "localhost";
$username   = "username";
$password   = "password";
$dbname     = "db";

// Create connection
$conn = new mysqli( $servername, $username, $password, $dbname );

// Check connection
if ( $conn->connect_error ) {
    die( "Connection failed: " . $conn->connect_error );
}
```

## To Do

- Adicionar opção para recuperação de senha/usuário
- Adicionar página para criação de novos usuários
- Alterar o modelo da tabela "users" para aceitar opção de "tipo de usuário"

## Changelog

	0.0.2 - Adiciona arquivo para criar tabelas ao banco de dados automaticamente.