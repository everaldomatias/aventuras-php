<?php
$lnkvldtn = filter_var( $_GET['lnkvldtn'], FILTER_SANITIZE_STRING );
if ( ! empty( $lnkvldtn ) ) {

    $get_username = filter_var( $_GET['u'], FILTER_SANITIZE_STRING );
    $get_email = filter_var( $_GET['e'], FILTER_SANITIZE_STRING );
    
    // Required Settings
    include_once( "inc/settings.php" );

    $search_lnkvldtn = "SELECT id, username, email, pass, lnkvldtn, creation_date FROM users_validation WHERE lnkvldtn = '$lnkvldtn' LIMIT 1";
    $result_search_lnkvldtn = mysqli_query( $conn, $search_lnkvldtn );
    
    if ( mysqli_num_rows( $result_search_lnkvldtn ) > 0 ) {
        
        while( $row = mysqli_fetch_array( $result_search_lnkvldtn ) ) {

            $current_date = new DateTime( 'NOW' );
            $current_date = $current_date->format( 'Y-m-d H:i:s' );
            
            $creation_date = $row['creation_date'];
            $creation_date_24 = new DateTime( $row['creation_date'] );
            $creation_date_24->modify( '+1 day' );
            $expiration_date = $creation_date_24->format( 'Y-m-d H:i:s' );
            
            if ( $row['lnkvldtn'] == $lnkvldtn && $current_date <= $expiration_date ) {
            
                $username = $row['username'];
                $email = $row['email'];
                $pass = $row['pass'];
                $creation_date = $row['creation_date'];
                            
                $createUser = "INSERT INTO users ( `username`, `email`, `pass`, `user_since` ) VALUES ( $username, $email, $pass, $creation_date )";
                if ( mysqli_query( $conn, $createUser ) ) {
                
                    $deleteUserValidation = "DELETE FROM users_validation WHERE lnkvldtn = " . $lnkvldtn;
	                mysqli_query( $conn, $deleteUserValidation );                
                
                    $_SESSION['msgCreateUser'] = '<div class="col-lg-12 alert alert-success"><strong>Pronto.</strong> Usuário cadastrado e ativado com sucesso!</div>';
                } else {
                    $_SESSION['msgCreateUser'] = '<div class="col-lg-12 alert alert-danger"><strong>Erro ao ativar seu cadastro.</strong> Por favor tente novamente.</div>';
                }
                            
            } else {
                echo 'Estamos de olho!!';
                die();
            }
            
        }        
    
        // criar o usuário na tabela correta
        
        // redirecionar para a página de login
    }
    
}